#!/usr/bin/env python3
#coding utf-8

#Les modules importés
import time
import os 
import sys
from tkinter import *
from tkinter import messagebox
import pygame

#Le HERE
HERE = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0,HERE)
os.chdir(HERE)

#La version
mn_version = "1.0"
#La fenêtre
root = Tk()
  
root.geometry("300x250")
#root.config(bg="Grey")
root.title("Minuteur | Version :" +mn_version)
img = PhotoImage(file='icon.png')
root.iconphoto(True, img)


#Le système de minutage 
hour=StringVar()
minute=StringVar()
second=StringVar()
  
hour.set("00")
minute.set("00")
second.set("00")
""" ***** Les champs d'entrées ****"""
#Les heures
hourEntry= Entry(root, width=3, font=("Andika",18,""),
                 textvariable=hour)
hourEntry.place(x=80,y=20)
#Les minutes
minuteEntry= Entry(root, width=3, font=("Andika",18,""),
                   textvariable=minute)
minuteEntry.place(x=130,y=20)
#Les secondes
secondEntry= Entry(root, width=3, font=("Andika",18,""),
                   textvariable=second)
secondEntry.place(x=180,y=20)
  
  
def submit():
    try:
        
        
        temp = int(hour.get())*3600 + int(minute.get())*60 + int(second.get())
    except:
        print("S'il vous plait, entrez le temps à respecter")
    while temp >-1:
         
        
        mins,secs = divmod(temp,60) 
  
        
        
        
        hours=0
        if mins >60:
            """ 
            print("Trop grand !")
            messagebox.showinfo("Fenêtre de temps !", "Le temps est ÉCOULÉ ! Fin de l'exercice !")
            pass
            """
            hours, mins = divmod(mins, 60)
         
        
        
        hour.set("{0:2d}".format(hours))
        minute.set("{0:2d}".format(mins))
        second.set("{0:2d}".format(secs))
  
        
        
        root.update()
        time.sleep(1)
  
        
        
        if (temp == 0):
            pygame.mixer.init()
            pygame.mixer.music.load("alarm.ogg")
            pygame.mixer.music.play(10,0.0)
            messagebox.showinfo("Fenêtre de temps !", "Le temps est ÉCOULÉ ! Fin de l'exercice !")
            pygame.mixer.music.stop()

                     
        
        
        temp -= 1
 
root.bind('<space>', submit)
btn = Button(root, text='Démarrer',  bd='5',
             command= submit)
btn.place(x = 70,y = 120)
  
root.mainloop()
